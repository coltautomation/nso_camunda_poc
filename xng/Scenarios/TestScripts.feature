﻿Feature: TestScripts

@WFH-2038, @CI @Enkeltsok @Regression1
    Scenario:WFH-2038-Enkeltsøk - Søkemuligheter 4 - Saksid/Journalpostid
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T2038"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then get Sakid from currentsakcase
    And Enter "Sakid" in sak search field
    Then Select Enter
    Then verify Id "Sakid" should displayed in search list in the search list
    And Close the Search result window
    Then get the jp "Id" on "0" journal list
    And Enter "Jpid" in search field
    Then Select Enter
    And Close the Search result window
    Then close application process

 
 @WFH-T2073, @CI @Arkivsok @Regression1 
    Scenario:WFH-T2073-Arkivsøk 1 - Lagre et søkeresultat
    Given Case "WFH-T2073" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Verify saved searchItem "TitleSearch" exists in searchmenu
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list 
    And Click "Ja" button
    Then close application process
    
 @WFH-T2074, @CI @Arkivsok @Regression1
    Scenario:WFH-T2074-Arkivsøk 2 - Administrere og bruke lagrede søk
    Given Case "WFH-T2073" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    And Select saved searchItem "TitleSearch"
    Then Close the Saved SearchResultWindow
    Then Click NyttArkivSok and Select MineLagredSok 
    Then Select Administration Sok
    And Verify Search Result Window
    Then Select "Delete" Button on saved "TitleSearch" searches list 
    And Click "Ja" button
    Then close application process
  
   @WFH-T2292,  @CI @Arkivsok @Regression1
    Scenario:WFH-T2292-Arkivsøk - Søkemuligheter 2 - Sak - Diverse søk - Admin.Enhet - Dato - Id
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T2292"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then get Sakid from currentsakcase
    When User clicks search button
    Then Click Expand button Arkivsearch Document window
    Then Click Expand button Arkivsearch journalpost window
    Then Click Expand button Arkivsok Sak window
    Then Click Sak Avdelingtree button in Admenhet
    Then Select admin in Treeview Item
    And verify the sak adm enhet text box as "DF_Adm_Enh"
    Then Select Search Button
    And verify  Admin in Saksearchresult as "DOK" admin
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then Enter Sakid as"SakId"
    Then Select Search Button
    And verify sak id as "SakId" in sak Searchresult Window
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then Enter start Date in sakarkivsok
    Then Select Search Button
    And verify all sak date as entered "Date"
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then click greyicon field in sak and verify calender displayed
    Then select date from calender and verify selected date inserted in sak field
    And select saktodate from grey icon calender 
    Then Select Search Button
    And verify all sak date as entered "Date"
    Then Select Searchquery button
    And Clear button in ArkivSok window
    Then click blue icon calender in sak
    Then Click the Plusicon next to Order Value fields
    Then Verify new line OrderPrinciple and OrederValue
    Then Click the Minusicon next to Order Value fields
    And Verify new line OrderPrinciple and OrederValue Removed
    And Enter "Test" in search Title field
    Then Select Tell Button
    Then verify number of sak,journalpost and document
    And verify all title in searchresult window with word"Test"
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then Enter Sakid as"SakId"
    Then Select Search Button
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then click Expand button Arkivsearch Sak window
    Then close application process
    
    
    @WFH-T2285, @CI @Arkivsok @Regression1
    Scenario:WFH-T2285-Arkivsøk 3 - Vise mitt lagrede søk i listen
    Given Case "WFH-T2285" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    Then Select Lagre Button
    Then Enter arkivSokName as "Test" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "CheckBox" Button on saved "TitleSearch" searches list 
    And Close the editStoreSearch window
    Then Click NyttArkivSok and Select MineLagredSok
    Then Verify saved searchItem "TitleSearch" not exists in searchmenu
    Then Select Administration Sok
    Then Select "CheckBox" Button on saved "TitleSearch" searches list
    And Close the editStoreSearch window
    Then Click NyttArkivSok and Select MineLagredSok
    Then Verify saved searchItem "TitleSearch" exists in searchmenu
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list 
    And Click "Ja" button
    Then Select "Delete" Button on saved "Test" searches list 
    And Click "Ja" button
    Then close application process
    
    @WFH-T2286, @CI @Arkivsok @Regression1
    Scenario:WFH-T2286-Arkivsøk 4 - Slette lagret søk
    Given Case "WFH-T2286" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    Then Select Lagre Button
    Then Enter arkivSokName as "Test" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list 
    And Click "Ja" button
    And Close the editStoreSearch window
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    And Verify "TitleSearch" not in EditstoreSerach window List
    Then Select "Delete" Button on saved "Test" searches list 
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
    
    @WFH-T2287, @CI @Arkivsok @Regression1
    Scenario:WFH-T2287-Arkivsøk 5 - Dele og slette søk
    Given Case "WFH-T2287" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok 
    Then Select Administration Sok
    Then Select "Share" Button on saved "TitleSearch" searches list
    And select "DF_journalRecepiant_ARK1" details
    Then Select "Share" Button on saved "TitleSearch" searches list
    And select "DF_journalRecepiant_LDR" details
    Then Verify Label Counter as "2" in sharing Button"TitleSearch"
    And Close the editStoreSearch window
    When logged out from Arkivar
    Then log in with Arkivar1
    Then Click NyttArkivSok and Select MineLagredSok
    Then Verify saved searchItem "TitleSearch" exists in searchmenu
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    When logged out from Arkivar
    Then login with Arkivar
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Verify Label Counter as "1" in sharing Button"TitleSearch"
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    Then close application process
    
    @WFH-T2294,  @CI @Arkivsok @Regression11
    Scenario:WFH-T2294-Arkivsøk 10 - Lagre kurv av lagret søk - Tilgang
    Given Case "WFH-T2294" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Kurv" Button on saved "TitleSearch" searches list
    And Select the Create Button
    Then close application process
    When Login with Admin
    Then Select "System" and Select "Kurver"
    And Select Nyttsok and Enter Kurver title as "TitleSearch"
    Then Click KurvSearch Button
    And Verify "TitleSearch" in Kurv Search result window
    Then Click and delete the "1" th Kurv Item
    Then close adminapplication process
    Then login with Arkivar
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
    
    
    @WFH-T2288, @CI @Arkivsok @Regression1
    Scenario:WFH-T2288-Arkivsøk 6 - Slette søk og eier av søk
    Given Case "WFH-T2288" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok 
    Then Select Administration Sok
    Then Select "Share" Button on saved "TitleSearch" searches list
    And select "DF_journalRecepiant_ARK1" details
    Then Verify Label Counter as "1" in sharing Button"TitleSearch"
    And Close the editStoreSearch window
    When logged out from Arkivar
    Then log in with Arkivar1
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Share" Button on saved "TitleSearch" searches list
    And select "DF_journalRecepiant_LDR" details
    Then Verify Label Counter as "2" in sharing Button"TitleSearch"
    And Close the editStoreSearch window
    When logged out from Arkivar
    Then login with Arkivar
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
    Then log in with Arkivar1
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Verify Label Counter as "1" in sharing Button"TitleSearch"
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
    
    
    @WFH-T2291, @CI @Arkivsok @Regression1
    Scenario:WFH-T2291-Arkivsøk 9 - Sortere på kolonner; Arkivsøk
    Given Case "WFH-T2291" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    And Select saved searchItem "TitleSearch"
    Then Click "SakDate"
    Then verify "SakDate_Date" in search results are sorted in "ASC" orders
    Then Click "SakDate"
    And verify "SakDate_Date" in search results are sorted in "DSC" 
    Then Click "SakId"
    Then verify "sakId_Number" in search results are sorted in "ASC" orders
    Then Click "SakId"
    Then verify "sakId_Number" in search results are sorted in "DSC" orders
    Then Close the Saved SearchResultWindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
    
    @WFH-T2289, @CI @Arkivsok @Regression1
    Scenario:WFH-T2289-Arkivsøk 7 -  Lage kurv av lagret søk - Ikke tilgang
    When logged out from Arkivar
    Then login with Saksbehandler
    Given Case "WFH-T2284" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Verify Kurv option is not available in ListBox
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
    
    @WFH-T2290,@CI @Arkivsok @Regression11
    Scenario:WFH-T2290- Arkivsøk 8 - Distribuere kurv til deg selv
    Given Case "WFH-T2290" should be created
    And Select NyttArkivSok in searchMenu
    And Enter Title in SearchTitleWindow
    Then Select Lagre Button
    Then Enter Name as "TitleSearch" and Click Lagre
    And Close the Searchwindow
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Kurv" Button on saved "TitleSearch" searches list
    #And Click distribute Kurv Check Box
    And Select the Create Button
    Then close application process
    When Login with Admin
    Then Select "System" and Select "Kurver"
    And Select Nyttsok and Enter Kurver title as "TitleSearch"
    #Then select Distribution share button
    Then Click KurvSearch Button
    Then close adminapplication process
    Then login with Arkivar
    #Then verify Kuruvar "TitleSearch" in Basket List
    When logged out from Arkivar
    When Login with Admin
    Then Select "System" and Select "Kurver"
    And Select Nyttsok and Enter Kurver title as "TitleSearch"
    #Then select Distribution share button
    Then Click KurvSearch Button
    And Verify "TitleSearch" in Kurv Search result window
    #And Verify "TitleSearch" in distributionKurv Search result window
    #Then Select and delete the distributed kuruver
    Then Click and delete the "1" th Kurv Item
    Then close adminapplication process
    Then login with Arkivar
    Then Click NyttArkivSok and Select MineLagredSok
    Then Select Administration Sok
    Then Select "Delete" Button on saved "TitleSearch" searches list
    And Click "Ja" button
    And Close the editStoreSearch window
    Then close application process
   
  @WFH-T1681, @CI @Dokument @Regression1
    Scenario: WH-1681 -Opprette offentlig variant - Ikke kopier eksisterende - Importer fil
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1681"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then verify the variant status of the doc it should be "P"
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And I should click on "Offentlig variant"
    And Click "Nei" button
    Then verify the variant status of the doc it should be "P"
    Then expand the document
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And I should click on "Importer fil..."
    And Click "Ja" button
    And attach the document
    Then Verify document "Versjon" as "1" in "1" th record
    Then Verify document "Variant" as "O" in "1" th record
    Then close application process
    
  @WFH-WFH-T1759, @CI @Avskrivning @Regression1
    Scenario Outline:  WFH-WFH-T1759 - Avskrivning inngående 3: Besvare flere inngående jp med ny utgående jp
    Given Case "WFH-T1759" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1759-1" as "Title1" for the document to be added
    Then Enter "WFH-T1759-1" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_sakStatus_S"
    And click Lagre button
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1759-2" as "Title1" for the document to be added
    Then Enter "WFH-T1759-2" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_sakStatus_M"
    And click Lagre button
    And Rightclick "1" th row in journal list
    And select avskriving
    And select Avskrivningsmate as "DF_Avskrivningsmate_BU"
    And Click avskrnving ok button
    Then Enter "WFH-T1759-3" as "Title1" for the document to be added
    Then Enter "WFH-T1759-3" as "Title2" for the document to be added
    And click journal Lagre button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    And Clicks "DF_journalDoc_Templete" document
    And clicks Avbryt Button
    And Clicks "sjekkInnOgAvslutt" button
    And click Lagre button
    And refresh maintreeview panel
    And Click first "Mine aktive saker" item
    Then Verify document "RegistrertAv" as "DF_RegistrertAv" in "0" th record
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    Then verify "JP Status" as "DF_jpStatus_M" in the "1" journal list
    Then verify "JP Status" as "DF_jpStatus_S" in the "2" journal list
    #Then verify "Tilgang" as "DF_Tilgang" in the "0" journal list    
    #Then verify "Tilgang" as "DF_Tilgang" in the "1" journal list
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    And Select "1" th row in journal list
    And select status as "DF_sakStatus_E"
    And click Lagre button    
    Then verify "JP Status" as "DF_jpStatus_E" in "0" journal list    
    Then verify "AvskrivningsInfo" as "DF_Avs_BU" in "1" th record
    Then verify "AvskrivningsInfo" as "DF_Avs_BU" in "2" th record    
    Then verify "JPType" as "DF_jpType_U" in the "0" journal list
    Then verify "JPType" as "DF_jpType_I" in the "2" journal list   
    Then close application process 
    
    Examples:
    | docType | caseCreation |
    | Inngående dokument  | WFH-T1759~Standard sak~DOK - Dokumentkontoret - Organ Test1~STD - Standard sak~TA_ARK - TestAutomation - Arkivar~DIR - Direktoratet~R - Reservert sak~SAK - Saksarkiv |
    
  @WFH-T1680, @CI @Dokument @Regression1 
    Scenario: WFH-T1680 - Opprette offentlig variant - Kopier eksisterende fil
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1680"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then verify the variant status of the doc it should be "P"
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And I should click on "Offentlig variant"
    And Click "Ja" button
    Then expand the document
    Then Verify document "Versjon" as "1" in "1" th record
    Then Verify document "Variant" as "O" in "1" th record
    Then close application process 
    
    
  @WFH-T1678, @CI @Dokument @Regression1
    Scenario: WH-1678 -Opprette ny versjon - Ikke kopier eksisterende - importer fil
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1678"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then verify the variant status of the doc it should be "P"
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And I should click on "Versjon..."
    And Click "Nei" button
    And select the journal document
    Then expand the document
    Then Verify document "Versjon" as "2" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And I should click on "Importer fil..."
    And attach the document
    Then Verify document "Versjon" as "2" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    Then close application process

    @WFH-T1685, @CI @Enkeltsok @Regression1
    Scenario Outline: WFH-T1685 - Enkeltsøk - Søk etter sak
    Given Case "WFH-T1685" should be created
    When User clicks search button
    Then Search results should be displayed
    
    Examples:
    | caseCreation  |
    | WFH-T1685~Standard sak~DOK - Dokumentkontoret - Organ Test1~STD - Standard sak~TA_ARK - TestAutomation - Arkivar~DIR - Direktoratet~R - Reservert sak~SAK - Saksarkiv |
    
    @WFH-T1686, @CI @Enkeltsok @Regression1
    Scenario Outline: WFH-T1686 - Enkeltsøk - Søk etter journalpost
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1686"
    When User clicks search button
    And Journal Search results should be performed
    Then Journal search results should be displayed

    Examples:
    | journalCreation |
    | WFH-T1686~TA_ARK~Utgående dokument |
    
   @WFH-T1687
   Scenario Outline: WFH-T1687 - 8 - Ekspedering - E-post
   Given Case "WFH-T1687" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T1687" as "Title1" for the document to be added
   Then Enter "WFH-T1687" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   And Clicks "DF_journalDoc_Blank" document
   And Clicks "sjekkInnOgAvslutt" button
   Then I should verify sender and receiver details as"DF_MottakarSak"
   #Then I should Click on EkSpeeder Link
   Then I should able to preview email being sent
   Then verify email content
   Then Verify jpStatus and forsendelsestatus
   #Then close application process 

   Examples:
    
    | docType | title1 | title2 |
    | Utgående dokument | title1 | title2 |
    
    
 @WFH-T1682, @CI @Dokument @Regression1
  Scenario: WFH-T1682 - Opprette arkivvariant - Konverter til PDF
  Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1682"
  When I attach "existing" document at the bottom
  Then document should be attached
  Then verify the variant status of the doc it should be "P"
  And I should click on "Arkivvariant"
  And Click YES on the alert message
  And click the journal expand
  Then verify the variant status of the doc it should be "A"
  Then close application process  
  
  @WFH-T1688, @CI @Ekspedering @Regression1
  Scenario Outline: WFH-T1688 -8 - Ekspedering - SDP (Sikker Digital Postkasse)
  Given Case "WFH-T1688" should be created
  Then Choose "DF_JournalMenuOutgoing" document from document list
  Then Enter "WFH-T1688" as "Title1" for the document to be added
  Then Enter "WFH-T1688" as "Title2" for the document to be added
  And click on journal avsendre button
  And select "DF_journalRecepiant_SDP" details
  And Click on Save button
  And Navigate to "sakdokumenterTab" tab
  And Navigate to "tabOpprettDokument" tab
  And Clicks "DF_journalDoc_Blank" document
  And Clicks "sjekkInnOgAvslutt" button
  Then check in the document
  Then verify "JP Status" as "DF_journalStatus_R" in the "0" journal list
  And select status as "DF_sakStatus_F"
  And click Lagre button
  Then verify "JP Status" as "DF_sakStatus_F" in the "0" journal list
  Then I should verify sender and receiver details as"DF_journalRecepiant_SDP"
  And Rightclick "1" th row and select Ekspeeder
  Then I should able to preview SDP
  Then verify "JP Status" as "DF_jpStatus_E" in the "0" journal list
  When logged out from Arkivar
  Then login with Arkivar
  Then select the Sender / Recipients tab
  Then verify "Ekspedrinstatus" as "DF_Esk_Sdp" in "0" Motakarlist
  And Navigate to "tabJournalPost" tab
  Then select history icon and press F5
  Then verify "JP Status" as "DF_jpStatus_E" in the "0" journal list
  Then close application process
  
  Examples:
    
    | docType | title1 | title2 |
    | Utgående dokument | title1 | title2 |

  
 @WFH-T1677, @CI @Dokument @Regression1
    Scenario: WH-1677 -Opprette ny versjon - Kopier eksisterende
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1677"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then verify the variant status of the doc it should be "P"
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And I should click on "Versjon..."
    And Click "Ja" button
    And select the journal document
    Then expand the document
    Then Verify document "Versjon" as "2" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    Then close application process
    
  @WFH-T1684, @CI @Journalpost @Regression1
    Scenario Outline: WFH-T1684 -Journalpost opprette - Dokumenttype U - Ekstern mottaker - Ny mottaker
    Given Case "WFH-T1684" should be created
    Then Choose "DF_JournalMenuOutgoing" document from document list
    Then Enter "WFH-T1684" as "Title1" for the document to be added
    Then Enter "WFH-T1684" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select the Sender / Recipients tab
    Then select New button
    Then Enter "Navn" as "DF_NewRecepiant_Name" for new external recepiant
    Then Enter "Adresse" as "DF_NewRecepiant_Address" for new external recepiant
    Then Enter "Epost" as "DF_NewRecepiant_Email" for new external recepiant
    And select on OK button
    And Navigate to "tabJournalPost" tab
    Then Verify "avdeling" in journal metadata as "DF_caseAdmenhet"
    Then Verify "saksbehandler" in journal metadata as "DF_caseSaksansvarling"
    Then Verify "Status" in journal metadata as "DF_MetaJpStatus_R"
    Then Verify "Type" in journal metadata as "DF_MetaJpType_U"
    Then Verify "JournalDatePicker" in journal metadata as "currentDate"
    Then Verify "DocDatePicker" in journal metadata as "currentDate" 
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify "DokNumber" as "1" in "0" journal list
    Then verify "Id" as "Not Null" in "0" journal list
    Then select the Sender / Recipients tab
    Then verify "NavnMedOrgan" as "DF_MottakarSak" in "0" Mottakar list 
    Then close application process
  
    Examples:
    | docType | title1 | title2 | recipiant |
    | Utgående dokument | WH1684 | title2 | TA_ARK |

    
  @WFH-T1728, @CI @Dokument @Regression1
    Scenario Outline: WFH-T1728-Opprett dokument - Importer fil
    Given Case "WFH-T1728" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    And Enter "WFH-T1728" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    Then verify the variant status of the doc it should be "P"
    Then verify the journal created sucessfully
  
    Examples:
    | docType | title1 | title2 | recipiant |
    | Inngående dokument | title1 | title2 | TA_ARK |
    
    
  @WFH-T1675, @CI @Dokument @Regression1
   Scenario Outline: WFH-T1675 - Opprette dokument - ikke fletting
    Given Case "WFH-T1675" should be created
    Then Choose "DF_JournalMenuOutgoing" document from document list
    And Enter "WFH-T1675" for the document to be added
    And click on journal leggtil button
    And select "DF_journalRecepiant" details
    And Click on Save button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    And Clicks "DF_journalDoc_Templete_Blank" document
    And Enter "test1" in the document
    And Clicks "sjekkInnOgAvslutt" button
    #Then check in the document
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And Open the created document
    And Enter "test1" text in the document search
    And Clicks "sjekkInnOgAvslutt" button
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    #Then close application process  

    Examples:
    | docType | title1 | title2 | recipiant |
    | Utgående dokument | WH1675 | title2 | TA_ARK |
    
    
    @WFH-T1676, @CI @Dokument @Regression1
    Scenario Outline:  WFH-T1676 -Opprette dokument - fletting
    Given Case "WFH-T1676" should be created
    Then Choose "DF_JournalMenuOutgoing" document from document list
    And Enter "WFH-T1676" for the document to be added
    And click on journal leggtil button
    And select "DF_journalRecepiant" details
    And Click on Save button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    And Clicks "DF_journalDoc_Templete" document
    And clicks Avbryt Button
    And Clicks "sjekkInnOgAvslutt" button
    #Then check in the document
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    And click Lagre button
    #And Select "1" th row in journal list
    And Open the created document
    And Enter "DF_journalDoc_Templete_Name" text in the document search
    And Enter "DF_journalDoc_Templete_PO" text in the document search
    And Enter "DF_journalDoc_Templete_Add" text in the document search
    And Enter "DF_journalDoc_Templete_Country" text in the document search
    And Clicks "sjekkInnOgAvslutt" button
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record
    #Then close application process      
    
    Examples:
    | docType | title1 | title2 | recipiant |
    | Utgående dokument | WFH-T1676 | title2 | CHE_TEST |

       
    @WFH-T1679, @CI @Dokument @Regression1
    Scenario Outline:  WFH-T1679 -Opprette ny versjon - Ved redigering av dokument
    Given Case "WFH-T1679" should be created
    Then Choose "DF_JournalMenuOutgoing" document from document list
    And Enter "WFH-T1679" for the document to be added
    And click on journal leggtil button
    And select "DF_journalRecepiant" details
    And Click on Save button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    Then Clicks "DF_journalDoc_Blank" document
    And clicks Avbryt Button
    And Enter "test1" in the document
    And Clicks "sjekkInnOgAvslutt" button
    Then check in the document
    Then Verify document "Versjon" as "1" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record 
    And Open the created document
    And Clicks LagreSomNyVersjon button
    Then Verify document "Versjon" as "2" in "0" th record
    Then Verify document "Variant" as "P" in "0" th record 
    Then close application process     
    
    Examples:
    | docType | title1 | title2 | recipiant |
    | Utgående dokument | WFH-T1679 | title2 | CHE_TEST |
    
   @WFH-T1758, @CI @Avskrivning @Regression1
    Scenario Outline:  WFH-T1758 - Avskrivning inngående 2: Besvare med ny utgående journalpost fra kurv
    Given Case "WFH-T1758" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1758" as "Title1" for the document to be added
    Then Enter "WFH-T1758" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    #And Navigate to "sakdokumenterTab" tab
    #Then select maintreeview refresh button
    #And Select "Til behandling" and "Innboks for inngående journalposter"
    And Rightclick "1" th row in journal list
    And select avskriving
    And select Avskrivningsmate as "DF_Avskrivningsmate_BU"
    And Click avskrnving ok button
    And click journal Lagre button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    Then Clicks "DF_journalDoc_Blank" document
    And Enter "test1" in the document
    And Clicks "sjekkInnOgAvslutt" button
    Then check in the document
    And click Lagre button
    Then Verify document "RegistrertAv" as "DF_RegistrertAv" in "0" th record
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    Then verify "JP Status" as "DF_jpStatus_J" in the "1" journal list
    Then verify "Tilgang" as "DF_Tilgang" in the "0" journal list    
    Then verify "Tilgang" as "DF_Tilgang" in the "1" journal list
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    And Navigate to "tabJournalPost" tab
    And select status as "DF_journalStatus_F"
    And click Lagre button
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    Then verify "JPType" as "DF_jpType_U" in "0" journal list    
    Then verify "AvskrivningsInfo" as "DF_Avs_BU" in "1" th record
    Then verify "JPType" as "DF_jpType_U" in the "0" journal list
    Then verify "JPType" as "DF_jpType_I" in the "1" journal list
    Then close application process


    Examples:
    | docType |
    | Inngående dokument  |


    @WFH-T1760, @CI @Avskrivning @Regression2
    Scenario Outline:  WFH1760 - Avskrivning inngående 4: Besvare en inngående med flere avsendere
    Given Case "WFH-T1760" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1760-1" as "Title1" for the document to be added
    Then Enter "WFH-T1760-1" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And click on journal avsendre button
    And select "DF_journalRecepiant_2" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    And Rightclick "1" th row in journal list
    And select avskriving
    And select Avskrivningsmate as "DF_Avskrivningsmate_BU"
    And Click avskrnving ok button
    Then click journal Lagre button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    And Clicks "DF_journalDoc_Templete" document
    And clicks Avbryt Button
    And Enter "test1" in the document
    And Clicks "sjekkInnOgAvslutt" button
    Then check in the document
    And click Lagre button
    Then Verify document "RegistrertAv" as "DF_RegistrertAv" in "0" th record
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    Then verify "JP Status" as "DF_jpStatus_J" in the "1" journal list
    Then verify "Tilgang" as "DF_Tilgang" in the "0" journal list
    Then verify "Tilgang" as "DF_Tilgang" in the "1" journal list
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    And Select "1" th row in journal list
    And select status as "DF_sakStatus_F"
    And click Lagre button
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    Then verify "JPType" as "DF_jpType_U" in "0" journal list    
    Then verify "AvskrivningsInfo" as "DF_Avs_BU" in "1" th record
    Then verify "JPType" as "DF_jpType_U" in the "0" journal list
    Then verify "JPType" as "DF_jpType_I" in the "1" journal list
    Then close application process
    
        
    Examples:
    | docType |
    | Inngående dokument  |
    
    @WFH-T1761, @CI @Avskrivning @Regression2
    Scenario Outline:  WFH-T1761 - Avskrivning inngående 5: Besvare en inngående med et eksisterende utgående
    Given Case "WFH-T1761" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1761-1" as "Title1" for the document to be added
    Then Enter "WFH-T1761-1" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    #And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1761-2" as "Title1" for the document to be added
    Then Enter "WFH-T1761-2" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1761-3" as "Title1" for the document to be added
    Then Enter "WFH-T1761-3" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    Then Choose "DF_JournalMenuOutgoing" document from document list
    Then Enter "WFH-T1761-4" as "Title1" for the document to be added
    Then Enter "WFH-T1761-4" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_R"
    And click Lagre button
    Then Choose "DF_JournalMenuOutgoing" document from document list
    Then Enter "WFH-T1761-5" as "Title1" for the document to be added
    Then Enter "WFH-T1761-5" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_R"
    And click Lagre button
    And Rightclick "4" th row in journal list
    And select avskriving
    And select Avskrivningsmate as "DF_Avskrivningsmate_BU"
    And Select existing Outgoing document
    And Click avskrnving ok button
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "3" journal list
    And Select "1" th row in journal list
    And select status as "DF_journalStatus_F"
    And click Lagre button
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "3" journal list
    Then verify "JPType" as "DF_jpType_U" in "0" journal list    
    Then verify "AvskrivningsInfo" as "DF_Avs_BU" in "3" th record
    Then close application process
    
        
    
    Examples:
    | docType | docType1 |
    | Inngående dokument  | Utgående dokument | 

    

    @WFH-T1762, @CI @Avskrivning @Regression2
    Scenario Outline:  WFH-T1762 - Avskrivning inngående 6: Besvare en inngående med TE- Tatt til etteretning
    Given Case "WFH-T1762" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1762" as "Title1" for the document to be added
    Then Enter "WFH-T1762" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select status as "DF_journalStatus_J"
    And click Lagre button
    And Rightclick "1" th row in journal list
    And select avskriving
    And select Avskrivningsmate as "DF_Avskrivningsmate_TE"
    And enter comment as "test comment"
    And Click avskrnving ok button
    And Refresh JournalPost
    And click on Journal Comment
    And Verify merknader comment as "test comment"
    And Rightclick "1" th row in journal list
    And refresh basket
    Then close application process



Examples:
    | docType | docType1 |
    | Inngående dokument  | Utgående dokument | 


    @WFH-T1763, @CI @Avskrivning @Regression2
    Scenario Outline:  WFH-T1763 - Avskrivning inngående 7: Besvare to inngående med US- Behandlet uten svardokument
    Given Case "WFH-T1763" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1763-1" as "Title1" for the document to be added
    Then Enter "WFH-T1763-1" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_sakStatus_J"
    And click Lagre button
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1763-2" as "Title1" for the document to be added
    Then Enter "WFH-T1763-2" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    And Navigate to "sakdokumenterTab" tab
    #And refresh maintreeview panel
    #And Select "Til behandling" and "Innboks for inngående journalposter"
    And Rightclick "1" th row in journal list
    And select avskriving
    And select "sak" in the Niva dropdown    
    And select Avskrivningsmate as "DF_Avskrivningsmate_US"
    And enter comment as "test comment"
    And Click avskrnving ok button
    And Navigate to "tabJournalPost" tab
    And Refresh JournalPost
    And click on Journal Comment
    And Verify merknader comment as "test comment"
    Then close application process

    Examples:
    | docType | docType1 |
    | Inngående dokument  | Utgående dokument | 
    
    
    @WFH-T1757, @CI @Avskrivning @Regression2
    Scenario Outline:  WFH-T1757 - Depreciation incoming 1: Reply with new outgoing journal entry
    Given Case "WFH-T1757" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T1757" as "Title1" for the document to be added
    Then Enter "WFH-T1757" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_journalStatus_J"
    And click Lagre button
    And Rightclick "1" th row in journal list
    And select avskriving
    And select Avskrivningsmate as "DF_Avskrivningsmate_BU"
    And Click avskrnving ok button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    And Clicks "DF_journalDoc_Templete" document
    And Click "Ja" button
    And clicks Avbryt Button
    And Clicks "sjekkInnOgAvslutt" button
    Then check in the document
    Then Verify document "RegistrertAv" as "DF_RegistrertAv" in "0" th record
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    Then verify "JP Status" as "DF_jpStatus_J" in the "1" journal list
    Then verify "Tilgang" as "DF_Tilgang" in the "0" journal list
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    And Select "1" th row in journal list
    And select status as "DF_journalStatus_F"
    And click Lagre button
    Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
    Then verify "JPType" as "DF_jpType_U" in "0" journal list    
    Then verify "AvskrivningsInfo" as "DF_Avs_BU" in "1" th record
    Then verify "JPType" as "DF_jpType_U" in the "0" journal list
    Then verify "JPType" as "DF_jpType_I" in the "1" journal list
    Then close application process
    
    
    Examples:
    | docType | docType1 |
    | Inngående dokument  | Utgående dokument |
    
    
    @WFH-T1683, @CI @Dokument @Regression2
    Scenario: WH-1683 -Opprette arkivvariant - Ikke konverter hoveddokument til PDF - Importert fil
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1683"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then verify the variant status of the doc it should be "P"
    And I should click on "Arkivvariant"
    And click NO on the alert message
    Then verify the variant status of the doc it should be "A"
    And collapse the journal
    And I should click on "Importer fil..."
    And attach the document "Test.pdf"
    Then verify the variant status of the doc it should be "P" in the "2" th record
    Then close application process

    
    @WFH-T1753
    Scenario Outline:  WFH-T1753 - Reply with new m note
    Given Case "WFH-1753" should be created
    Then Choose "DF_JournalMenuOutgoing_N" document from document list
    Then Enter "WFH-1753" as "Title1" for the document to be added
    Then Enter "WFH-1753" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant_ARK" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_sakStatus_J"
    And click Lagre button
    And Navigate to "sakdokumenterTab" tab
    And refresh maintreeview panel
    And Navigate to "tabJournalPost" tab
    And Select "Til behandling" and "Innboks for inngående journalposter"
    And Rightclick "1" th row in journal list
    And select avskriving
    And select "sak" in the Niva dropdown    
    And select Avskrivningsmate as "DF_Arkivisingmate_NN"
    And Select AvskrivningsmateDep2 as "DF_Nnotatm"
    And enter comment as "test comment"
    And Click avskrnving ok button
    Then click journal Lagre button
    Then close application process
    
    
    Examples:
    | docType | docType1 |
    | Inngående dokument  | Utgående dokument | 
    
    
    @WFH-T1754
    Scenario Outline:  WFH-T1754 - Reply with new x note
    Given Case "WFH-T1754" should be created
    Then Choose "DF_JournalMenuOutgoing_X" document from document list
    Then Enter "WFH-T1754" as "Title1" for the document to be added
    Then Enter "WFH-T1754" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    And select skjeming as "DF_journslTillgsng" and "DF_Avskjermingskode" and "DF_Paragraf"
    And select status as "DF_sakStatus_E"
    And click Lagre button
    And Navigate to "sakdokumenterTab" tab
    And refresh maintreeview panel
    And Navigate to "tabJournalPost" tab
    And Select "Til behandling" and "Innboks for inngående journalposter"
    #Then close application process
    
    Examples:
    | docType | docType1 |
    | Inngående dokument  | Utgående dokument | 
    
    
    @WFH-T1970, @CI @Journalpost @Regression2
    Scenario:  WFH-T1970 - Journalpost kopiere - Kopier for gjenbruk til samme sak
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1970"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select status as "DF_journalStatus_F"
    And click Lagre button
    And Rightclick "1" th row in journal list
    Then Select Organize, Copy for Reuse and select recent cases
    Then verify the alert appears to confirm copy
    And Click "Ja" button
    Then verify "Tittel" as "currentJpTitle" in "0" journal list 
    Then verify "Recepiant" as "DF_journalRecepiant_ARK" in "1" in journalinfo
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    And Navigate to "sakdokumenterTab" tab
    Then Verify document "Navn" as "currentcaseTitle" in "0" th record
    Then close application process
    
   
    @WFH-T1971, @CI @Journalpost @Regression2
    Scenario:  WFH-T1971 - Journalpost kopiere - Kopier for gjenbruk til ny sak
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1971"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select status as "DF_journalStatus_F"
    And click Lagre button
    And Rightclick "1" th row in journal list
    Then Select Organize, Copy for Reuse and select Standard Sak
    Then verify the alert appears to confirm "Standard sak"
    And Click "Ja" button
    Then enter Title "WFH-T1971" and "WFH-T1971" and save
    Then verify new case has been created successfully
    Then verify "Tittel" as "currentcaseTitle" in main window
    Then verify "Recepiant" as "DF_journalRecepiant_ARK" in "1" in journalinfo
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    And Navigate to "sakdokumenterTab" tab
    Then Verify document "Navn" as "currentcaseTitle" in main window
    Then close application process
    
    
    @WFH-T1972, @CI @Journalpost @Regression2
    Scenario:  WFH-T1972 - Journalpost kopiere - Kopier for gjenbruk - Søk etter sak
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1972"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select status as "DF_journalStatus_F"
    And click Lagre button
    And Rightclick "1" th row in journal list
    Then Select Organize, Copy for Reuse and select serach for case
    And Select search button in search window
    Then select the case to copy
    Then verify the alert appears to confirm copy
    And Click "Ja" button
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify "Recepiant" as "DF_journalRecepiant_ARK" in "1" in journalinfo
    Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
    And Navigate to "sakdokumenterTab" tab
    Then Verify document "Navn" as "currentcaseTitle" in "0" th record
    Then close application process
    
    @WFH-T1973, @CI @Journalpost @Regression2
    Scenario:  WFH-T1973 - Journalpost kopiere - Eksakt kopi til samme sak
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1973"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select status as "DF_journalStatus_F"
    And click Lagre button
    And Rightclick "1" th row in journal list
    Then Select Organize, Exact copy and select recent cases
    Then verify the alert appears to confirm copy
    And Click "Ja" button
    Then verify "Tittel" as "currentJpTitle" in "0" journal list 
    Then verify "Recepiant" as "DF_journalRecepiant_ARK" in "1" in journalinfo
    Then verify "JP Status" as "DF_jpStatus_F" in the "0" journal list
    And Navigate to "sakdokumenterTab" tab
    Then Verify document "Navn" as "currentcaseTitle" in "0" th record
    Then close application process
    
    @WFH-T1974, @CI @Journalpost @Regression2
    Scenario:  WFH-T1974 - Journalpost kopiere - Eksakt kopi til ny sak
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1974"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select status as "DF_journalStatus_F"
    And click Lagre button
    And Rightclick "1" th row in journal list
    Then Select Organize, Exact copy and select Standard sak
    Then verify the alert appears to confirm "Standard sak"
    And Click "Ja" button
    Then enter Title "WFH-T1974" and "WFH-T1974" and save
    Then verify "Tittel" as "currentcaseTitle" in main window
    Then verify "Recepiant" as "DF_journalRecepiant_ARK" in "1" in journalinfo
    Then verify "JP Status" as "DF_jpStatus_F" in the "0" journal list
    And Navigate to "sakdokumenterTab" tab
    Then Verify document "Navn" as "currentcaseTitle" in main window
    Then close application process
    
    @WFH-T1975, @CI @Journalpost @Regression2
    Scenario:  WFH-T1975 - Journalpost kopiere - Eksakt kopi - Søk etter sak
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1975"
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select status as "DF_journalStatus_F"
    And click Lagre button
    And Rightclick "1" th row in journal list
    Then Select Organize, Exact copy and select Search case
    And Select search button in search window
    Then select the case to copy
    Then verify the alert appears to confirm copy
    And Click "Ja" button
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify "Recepiant" as "DF_journalRecepiant_ARK" in "1" in journalinfo
    Then verify "JP Status" as "DF_jpStatus_F" in the "0" journal list
    And Navigate to "sakdokumenterTab" tab
    Then Verify document "Navn" as "currentcaseTitle" in "0" th record
    Then close application process

    
    @WFH-T2056, @CI @Journalpost @Regression2
    Scenario:  WFH-T2056 - Journalpost - Opprette - Dokumenttype I - En ekstern mottaker - Ny mottaker
    Given Case "WFH-T2056" should be created
    Then Choose "DF_JournalMenuIncoming" document from document list
    Then Enter "WFH-T2056" as "Title1" for the document to be added
    Then Enter "WFH-T2056" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant" details
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select the Sender / Recipients tab
    Then select New button
    Then Enter "Navn" as "DF_NewRecepiant_Name" for new external recepiant
    Then Enter "Adresse" as "DF_NewRecepiant_Address" for new external recepiant
    Then Enter "Epost" as "DF_NewRecepiant_Email" for new external recepiant
    And select on OK button
    And Navigate to "tabJournalPost" tab
    Then Verify "avdeling" in journal metadata as "DF_caseAdmenhet"
    Then Verify "saksbehandler" in journal metadata as "DF_caseSaksansvarling"
    Then Verify "Status" in journal metadata as "DF_MetaJpStatus"
    Then Verify "Type" in journal metadata as "DF_MetaJpType"
    Then Verify "Beh.type" in journal metadata as "DF_MetaBehandlingsType"
    Then Verify "JournalDatePicker" in journal metadata as "currentDate"
    Then Verify "DocDatePicker" in journal metadata as "currentDate"
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify "DokNumber" as "1" in "0" journal list
    Then verify "Id" as "Not Null" in "0" journal list
    Then select the Sender / Recipients tab
    Then verify "NavnMedOrgan" as "DF_MottakarSak" in "0" Mottakar list
    Then close application process
    
    @WFH-T2057, @CI @Journalpost @Regression2
    Scenario:WFH-T2057-Journalpost - Opprette - Dokumenttype N - Interne mottakere - En hovedmottaker og en kopimottaker
    Given Case "WFH-T2057" should be created
    Then Choose "DF_JournalMenuOutgoing_N" document from document list
    Then Enter "WFH-T2057" as "Title1" for the document to be added
    Then Enter "WFH-T2057" as "Title2" for the document to be added
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select the Sender / Recipients tab
    And select Leggitil button
    And select "DF_journalRecepiant" details
    And select Leggitil button
    And select "DF_journalRecepiant_2" details and Select Kopi button
    And Navigate to "tabJournalPost" tab
    Then Verify "avdeling" in journal metadata as "DF_caseAdmenhet"
    Then Verify "saksbehandler" in journal metadata as "DF_caseSaksansvarling"
    Then Verify "Status" in journal metadata as "DF_MetaJpStatus_R"
    Then Verify "Type" in journal metadata as "DF_MetaJpType_N"
    Then Verify "JournalDatePicker" in journal metadata as "currentDate"
    Then Verify "DocDatePicker" in journal metadata as "currentDate"
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify "DokNumber" as "1" in "0" journal list
    Then verify "Id" as "Not Null" in "0" journal list
    Then select the Sender / Recipients tab
    Then verify "NavnMedOrgan" as "DF_MottakarSak" in "0" Mottakar list
    Then verify "NavnMedOrgan" as "DF_MottakarSak1" in "1" Mottakar list
    Then close application process
    
     @WFH-T2058, @CI @Journalpost @Regression2
    Scenario:WFH-T2058-Journalpost - Opprette - Dokumenttype X - En intern mottaker
    Given Case "WFH-T2058" should be created
    Then Choose "DF_JournalMenuOutgoing_X" document from document list
    Then Enter "WFH-T2058" as "Title1" for the document to be added
    Then Enter "WFH-T2058" as "Title2" for the document to be added
    And Click on Save button
    When I attach "existing" document at the bottom
    Then document should be attached
    Then select the Sender / Recipients tab
    And select Leggitil button
    And select "DF_journalRecepiant" details
    And Navigate to "tabJournalPost" tab
    Then Verify "avdeling" in journal metadata as "DF_caseAdmenhet"
    Then Verify "saksbehandler" in journal metadata as "DF_caseSaksansvarling"
    Then Verify "Status" in journal metadata as "DF_MetaJpStatus_R"
    Then Verify "Type" in journal metadata as "DF_MetaJpType_X"
    Then Verify "JournalDatePicker" in journal metadata as "currentDate"
    Then Verify "DocDatePicker" in journal metadata as "currentDate"
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify "Id" as "Not Null" in "0" journal list
    Then select the Sender / Recipients tab
    Then verify "NavnMedOrgan" as "DF_MottakarSak" in "0" Mottakar list
    Then close application process
    
    @WFH-T1976, @CI @Journalpost @Regression2
    Scenario:WFH-T1976- Journalpost redigere - Endre dokumenttype
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T1976"
    When I attach "existing" document at the bottom
    Then document should be attached
    And Navigate to "tabJournalPost" tab
    Then Select Field type and select different document"notat"
    And click Lagre button
    Then verify "JPType" as "DF_JpType_N" in "0" journal list
    Then close application process
    
    
    @WFH-T2035, @CI @Enkeltsok @Regression2
    Scenario:WFH-T2035- Enkeltsøk - Søkemuligheter 1 - Sakstittel = Knut Kåre
    Given Case "Knut Kåre er best" should be created
    And Enter "Knut" in search field 
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "Q_Knut" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "Q_knut kåre" in search field and press Enter
    And Enter "best" in search field
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "Q_Knut kåre" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "Q_knut best" in search field and press Enter
    And Enter "kåre" in search field
    Then Select Enter
    Then verify "currentcaseTitle" should not displayed in search list in the search list
    And Close the Search result window
    And Enter "kåre" in search field
    And Enter "Q_knut best" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should not displayed in search list in the search list
    And Close the Search result window
    And Enter "kåre -best" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should not displayed in search list in the search list
    Then close application process
    
    @WFH-T2036, @CI @Enkeltsok @Regression2
    Scenario:WFH-T2036-Enkeltsøk - Søkemuligheter 2 - Sakstittel = Jul i Skomakergata - Sakstype = Standard -  Klassering = NAVADM og Ordningsverdi = 21
    Given Case Title "Knut Kåre er best" should be created
    Then Select Classification "DF_Classification" and Order value"DF_OrderValue"
    And click on Lagre to create a case 
    And Enter "Knut Kåre" in search field and press Enter
    Then Select Enter
    And Close the Search result window
    And Enter "Knut Kåre" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "Q_Knut" in search field and press Enter
    And Enter "Q_Kåre" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "NAVADM:21" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    And Enter "NAVADM:21 Knut" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    Then select Search Icon 
    And Select Advanced Search
    Then Enter "SakTitle" as "DF_Journal_Knut" and Select Search icon
    Then verify "currentcaseSTitle" should displayed in search list in the search list
    Then Enter "Saktype" as "DF_Saktype" and Select Search icon
    Then verify "currentcaseTitle" should displayed in search list in the search list
    And Close the Search result window
    Then close application process
    
    @WFH-T2070, @CI @Enkeltsok @Regression2
    Scenario:WFH-T2070-Enkeltsøk - Sortere på kolonner - Tittel
    And Enter "test" in search field 
    Then Select Enter
    And select Title
    Then verify "TitleSearchResults" in search results are sorted in "ASC" order
    And select Title
    Then verify "TitleSearchResults" in search results are sorted in "DSC" order
    Then close application process
    
    
    @WFH-T2037, @CI @Enkeltsok @Regression2
    Scenario:WFH-T2037-Enkeltsøk - Søkemuligheter 3 - Journalpost 
    Given Case "søknad" should be created
    Then Choose "DF_JournalMenuOutgoing" document from document list
    Then Enter "søknad" as "Title1" for the document to be added
    Then Enter "søknad" as "Title2" for the document to be added
    And Click on Save button
    Then select the Sender / Recipients tab
    Then select New button
    Then Enter "Navn" as "DF_NewRecepiant_Name" for new external recepiant
    Then Enter "Epost" as "DF_NewRecepiant_Email" for new external recepiant
    Then Enter "Attention" as "DF_NewRecepiant_Attention" for new external recepiant
    Then Enter "Reference" as "DF_NewRecepiant_Reference" for new external recepiant
    And select on OK button
    And click on Lagre button
    And Enter "DF_NewRecepiant_Name" in search field and press Enter
    And Close the Search result window
    And Enter "DF_NewRecepiant_Name" in search field and press Enter
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    And Close the Search result window
    And Enter "DF_NewRecepiant_Email" in search field and press Enter
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    And Close the Search result window
    And Enter "Ektefelle" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    And Close the Search result window
    And Enter "Ola Testcomplete" in search field and press Enter
    Then Select Enter
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    And Close the Search result window
    Then select Search Icon 
    And Select Advanced Search
    Then Enter "JournalTitle" as "DF_Journal_Title" and Select Search icon
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    And Remove value in Advanced search Field
    Then Enter "JournalType" as "DF_jpType_U" and Select Search icon
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    And Remove value in Advanced search Field
    Then Enter "Recepiant" as "DF_NewRecepiant_Name" and Select Search icon
    Then verify "currentcaseTitle" of JP should displayed in search list in the search list
    Then close application process
    
   
  @WFH-T2072, @CI @Arkivsok @Regression2
    Scenario:WFH-T2072-Arkivsøk - Søkemuligheter 1 - Sakstittel = Knut Kåre
    Given Case "Knut Kåre er best" should be created
    When User clicks search button
    And Enter "Kåre best" in search Title field and Select Search
    Then Select Search Button
    Then verify "currentcaseTitle" should displayed in search list in the Arkiv search list
    Then Close the Saved SearchResultWindow
    When User clicks search button
    And Enter "Kåre + best" in search Title field and Select Search
    Then Select Search Button
    Then verify "currentcaseTitle" should displayed in search list in the Arkiv search list
    Then Close the Saved SearchResultWindow
    When User clicks search button
    And Enter "kåre +best +kode" in search Title field and Select Search
    Then Select Search Button
    And verify Search List not found
    And Close the Searchwindow
    When User clicks search button
    And Enter "kåre -best" in search Title field and Select Search
    Then Select Search Button
    And verify Search List not found
    And Close the Searchwindow
    When User clicks search button
    And Enter "Q_Knut" in search Title field and Select Search
    Then Select Search Button
    Then verify "currentcaseTitle" should displayed in search list in the Arkiv search list
    Then Close the Saved SearchResultWindow
    When User clicks search button
    And Enter "Q_Knut Kåre" in search Title field and Select Search
    And Enter "best" in search Title field and Select Search
    Then Select Search Button
    Then verify "currentcaseTitle" should displayed in search list in the Arkiv search list
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    And Enter "Q_Knut best" in search Title field 
    Then Select Search Button
    And verify Search List not found
    And Clear button in ArkivSok window
    And Enter "Q_Knut best" in search Title field
    Then Select Search Button
    And verify Search List not found
    And Clear button in ArkivSok window
    And Enter "Q_Knut Kåre" in search Title field
    Then Select Search Button
    Then verify "currentcaseTitle" should displayed in search list in the Arkiv search list
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    And Enter "Q_Knut * best" in search Title field 
    Then Select Search Button
    Then verify "currentcaseTitle" should displayed in search list in the Arkiv search list
    And verify all title in searchresult window with word"Knut"
    Then close application process
    
      
    @WFH-T2355,  @CI @Arkivsok @Regression2
    Scenario:WFH-T2355-Arkivsøk - Søkemuligheter 3 - Journalpost - Diverse søk - Admin.Enhet - Dato - Tittel - Id
    Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T2355_test"
    Then get the jp "Id" on "0" journal list
    When User clicks search button
    Then Click Expand button Arkivsearch Document window
    Then click Expand button Arkivsearch Sak window
    Then Click Expand button Arkivsok journalpost window
    And Enter "Test" in Journalpost title for search
    Then Select Search Button
    And verify all JP title in searchresult window with word"Test"
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then Click Avdelingtree button in Adm.enhet
    Then Select admin in Treeview Item
    And verify the adm enhet text box as "DF_Adm_Enh"
    Then Select Search Button
    And verify Admin in Jpsearchresult as "DOK" admin
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then Enter start Date in Jp
    Then Select Search Button
    And verify all jp date as entered "Date"
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    Then click greyicon field and verify calender displayed
    Then select date from calender and verify selected date inserted in Jp field
    And select todate from grey icon calender
    Then Select Search Button
    And verify all jp date as entered "Date"
    Then Select Searchquery button
    And Clear button in ArkivSok window
    Then Enter JournalPost Jp id
    Then Select Search Button
    And verify Jpsak id as "Id" in jp Searchresult Window
    Then Select Searchquery button 
    And Clear button in ArkivSok window
    And Select Doctype Plus icon and verify the window
    Then Select AveMot Symbol in Jp and verify Searchresult window
    Then Click Expand button Arkivsearch journalpost window
    Then close application process
    
   @WFH-T2356,  @CI @Arkivsok @Regression2
   Scenario:WFH-T2356-Arkivsøk - Søkemuligheter 4 - Dokument - Diverse søk - Dato - Tittel - Id
   Given Journal Entry is created with "DF_journalRecepiant" and "WFH-T2356"
   When I attach "existing" document at the bottom
   Then document should be attached
   Then get the Document "Dokid" on "0" journal list
   When User clicks search button
   Then click Expand button Arkivsearch Sak window
   Then Click Expand button Arkivsearch journalpost window
   Then Click Expand button Arkivsok Document window
   And Enter "Test" in Document title for search
   Then Select Search Button
   Then Select Searchquery button 
   And Clear button in ArkivSok window
   Then Enter start Date in Document
   Then Select Search Button
   And verify all document date as entered "Date"
   Then Select Searchquery button 
   And Clear button in ArkivSok window
   Then click greyicon field in doc and verify calender displayed
   Then select date from calender and verify selected date inserted in Doc field
   And select registertodate from grey icon calender
   Then Select Search Button
   And verify all document date as entered "Date"
   Then Select Searchquery button
   And Clear button in ArkivSok window
   Then Enter Document Search Id as "Dokid"
   Then Select Search Button
   And verify  Docid as "Dokid" in document Searchresult Window
   Then Select Searchquery button 
   And Clear button in ArkivSok window
   Then Click the Icon next to Skjerming DocSearch Window
   Then Verify Skjerming window appear
   And Cancel the Skjerming window
   Then Click Expand button Arkivsearch Document window
   Then close application process 
     
    @WFH-T1689, @CI @Ekspedering @Regression2
   Scenario:WFH-T1689-Send til - E-post
   Given Case "WFH-T1689" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T1689" as "Title1" for the document to be added
   Then Enter "WFH-T1689" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   Then select import tab
   And attach the document "Test.pdf"
   Then document should be attached
   And Rightclick "1" th row in journal list
   Then Select Epost in menu
   And click ok button in conversion window
   Then enter the recepiant email as"DF_Outlook_Mail" and select send
   Then verify email content
   Then close application process
   
  @WFH-T1764
   Scenario:WFH-T1764-Depreciation N-note 1: Answer with a new N-note
   Given Case "WFH-T1764" should be created
   Then Choose "DF_JournalMenuOutgoing_N" document from document list
   Then Enter "WFH-T1764" as "Title1" for the document to be added
   Then Enter "WFH-T1764" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And Click on Save button
   When I attach "existing" document at the bottom
   Then document should be attached
   And select status as "DF_journalStatus_J"
   And click Lagre button
   When logged out from Arkivar
   Then log in with saksbehandler
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   Then verify "Tittel" as "currentJpTitle" in "0" journal list
   And Rightclick "1" th row in journal list
   And select avskriving
   Then verify "currentcaseTitle" as selected in Avskrivningsmate
   And select Avskrivningsmate as "DR_Arkivisingmate_NN"
   And verify Avskrivningsmate dependent field as "Nytt notat" and "Etatsinternt notat m/oppfølging"
   And Click avskrnving ok button
   Then Enter "WFH-T1764-2" as "Title1" for the document to be added
   Then Enter "WFH-T1764-2" as "Title2" for the document to be added
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   #And Clicks Notat document
   And Clicks "sjekkInnOgAvslutt" button
   Then Rightclick document and click checkin
   Then verify "JP Status" as "DF_jpStatus_R" in the "0" journal list
   Then verify "JP Status" as "DF_jpStatus_J" in the "1" journal list
   Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
   And Select "1" th row in journal list
   And Navigate to "tabJournalPost" tab
   And select status as "DF_journalStatus_F"
   And click Lagre button
   Then verify "JP Status" as "DF_jpStatus_F" in the "0" journal list
   Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   Then verify n notat basket "CurrentCasetitle" not in the list
   
   @WFH-T1765
   Scenario:WFH-T1765-Depreciation N-note 2: Answer with a new X-note
   When logged from Arkivar
   Then log in with saksbehandler
   Given Case "WFH-T1765" should be created
   Then Choose "DF_JournalMenuOutgoing_N" document from document list
   Then Enter "WFH-T1765" as "Title1" for the document to be added
   Then Enter "WFH-T1765" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And Click on Save button
   When I attach "existing" document at the bottom
   Then document should be attached
   And select status as "DF_sakStatus_E"
   And click Lagre button
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   And Rightclick "1" th row in journal list
   And select avskriving
   Then verify "currentcaseTitle" as selected in Avskrivningsmate
   And select Avskrivningsmate as "DF_Arkivisingmate_BX"
   And verify Avskrivningsmate dependent field as "Nytt notat uten oppfølging" and "X-notat u/ oppfølging"
   And Click avskrnving ok button
   Then Enter "WFH-T1765-2" as "Title1" for the document to be added
   Then Enter "WFH-T1765-2" as "Title2" for the document to be added
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   #And Clicks Notat document
   And Clicks "sjekkInnOgAvslutt" button
   Then Rightclick document and click checkin
   Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
   And Select "1" th row in journal list
   And Navigate to "tabJournalPost" tab
   And select status as "DF_sakStatus_E"
   And click Lagre button    
   Then verify "JP Status" as "DF_jpStatus_E" in "0" journal list    
   Then verify "AvskrivningsInfo" as "(BX) - Avskrevet" in "1" th record
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   Then verify n notat basket "CurrentCasetitle" not in the list
   
   @WFH-T1766
   Scenario:WFH-T1766-Depreciation N-note 3: Reply with new N-note, several recipients
   When logged from Arkivar
   Then log in with saksbehandler
   Given Case "WFH-T1766" should be created
   Then Choose "DF_JournalMenuOutgoing_N" document from document list
   Then Enter "WFH-T1766" as "Title1" for the document to be added
   Then Enter "WFH-T1766" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_2" details
   And Click on Save button
   When I attach "existing" document at the bottom
   Then document should be attached
   And select status as "DF_sakStatus_E"
   And click Lagre button
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   And Rightclick "1" th row in journal list
   And select avskriving
   And select Avskrivningsmate as "DR_Arkivisingmate_NN"
   And verify Avskrivningsmate dependent field as "Nytt notat" and "Etatsinternt notat m/oppfølging"
   And Click avskrnving ok button
   Then Enter "WFH-T1765-2" as "Title1" for the document to be added
   Then Enter "WFH-T1765-2" as "Title2" for the document to be added
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   #And Clicks Notat document
   #And Enter "test1" in the document
   And Clicks "sjekkInnOgAvslutt" button
   Then Rightclick document and click checkin
   Then verify "BesvarerJP" and "AvskrivningsInfo" in "0" and "1" journal list
   And Select "1" th row in journal list
   And select status as "DF_sakStatus_E"
   And click Lagre button    
   Then verify "JP Status" as "DF_jpStatus_E" in "0" journal list
   #Then verify "AvskrivningsInfo" as "(BX) - Avskrevet" in "1" th record
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   Then verify n notat basket "CurrentCasetitle" not in the list
   
  @WFH-T1767
   Scenario:WFH-T1766-Depreciation N-note 4: Answer with TE - Tatt til etterretning
   When logged from Arkivar
   Then login with Saksbehandler
   And refresh maintreeview panel
   And Select right "Til behandling" and "Innboks N-notater"
   And select avskriving
   Then verify "currentcaseTitle" as selected in Avskrivningsmate
   And select Avskrivningsmate as "TE - Tatt til etterretning"
   And Click avskrnving ok button
   
 @WFH-T2001, @CI @Ekspedering @Regression2
   Scenario:WFH-T2001-8 - Ekspedering - SvarUt - Dokumenttype U - En ekstern mottaker
   Given Case "WFH-T2001" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T2001" as "Title1" for the document to be added
   Then Enter "WFH-T2001" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant_SU" details
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   And Clicks "DF_journalDoc_Templete" document
   And clicks Avbryt Button
   And Clicks "sjekkInnOgAvslutt" button
   Then check in the document
   Then verify "JP Status" as "DF_journalStatus_R" in the "0" journal list
   And select status as "DF_sakStatus_F"
   And click Lagre button
   Then verify "JP Status" as "DF_sakStatus_F" in the "0" journal list
   And Rightclick "1" th row and select Ekspeeder
   Then Click the preview button 
   And click expand button verify document 
   Then click on the main document and verify pdfview window
   And click ekspeder button
   When logged out from Arkivar
   Then login with Arkivar
   Then select the Sender / Recipients tab
   Then verify "Ekspedrinstatus" as "DF_ESKpderstatus_Papir" in "0" Motakarlist
   Then verify "JP Status" as "DF_jpStatus_E" in the "0" journal list
   And Navigate to "tabJournalPost" tab
   Then select history icon and press F5
   Then close application process
   
   @WFH-T2002, @CI @Ekspedering @Regression2
   Scenario:WFH-T2002-8 - Ekspedering - Papir - Dokumenttype U - En ekstern hovedmottaker - En ekstern kopimottaker
   Given Case "WFH-T2002" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T2002" as "Title1" for the document to be added
   Then Enter "WFH-T2002" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_2" details and Select Kopi button
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   And Clicks "DF_journalDoc_Templete" document
   And clicks Avbryt Button
   And Clicks "sjekkInnOgAvslutt" button
   Then check in the document
   Then verify "JP Status" as "DF_journalStatus_R" in the "0" journal list
   And select status as "DF_journalStatus_F"
   And click Lagre button
   Then verify "JP Status" as "DF_journalStatus_F" in the "0" journal list
   And Rightclick "1" th row and select Ekspeeder
   Then select recepiant checkbox and the preview button
   And click expand button verify document 
   Then click on the main document and verify pdfview window
   And click ekspeder button
   Then verify "JP Status" as "DF_sakStatus_E" in the "0" journal list
   When logged out from Arkivar
   Then login with Arkivar
   Then select the Sender / Recipients tab
   Then verify "Ekspedrinstatus" as "DF_ESKpderstatus_Papir" in "0" Motakarlist
   Then verify "Ekspedrinstatus" as "DF_ESKpderstatus_Papir" in "1" Motakarlist
   Then close application process
   
   @WFH-T2742
   Scenario:WFH-T2742-8 -Dispatch- Document type N - Several internal main recipients and copy recipients 
   Given Case "WFH-T2742" should be created
   Then Choose "DF_JournalMenuOutgoing_N" document from document list
   Then Enter "WFH-T2742" as "Title1" for the document to be added
   Then Enter "WFH-T2742" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_ARK1" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_2" details and Select Kopi button
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   And Clicks "DF_journalDoc_Templete" document
   And clicks Avbryt Button
   And Clicks "sjekkInnOgAvslutt" button
   Then verify "JP Status" as "DF_journalStatus_R" in the "0" journal list
   
   @WFH-T2743, @CI @Ekspedering @Regression2
   Scenario:WFH-T2743-8 - Ekspedering - Flere omganger
   Given Case "WFH-T2743" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T2743" as "Title1" for the document to be added
   Then Enter "WFH-T2743" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_ARK1" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_AG" details and Select Kopi button
   And click on journal avsendre button
   And select "DF_journalRecepiant_LDR" details and Select Kopi button
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   When I attach "existing" document at the bottom
   Then document should be attached
   #And Navigate to "tabOpprettDokument" tab
   #And Clicks "DF_journalDoc_Templete" document
   #And clicks Avbryt Button
   #And Clicks "sjekkInnOgAvslutt" button
   #Then check in the document
   Then verify "JP Status" as "DF_journalStatus_R" in the "0" journal list
   And Rightclick "1" th row and select Ekspeeder
   Then I should able to preview Epost 
   And click expand button verify document 
   Then click on the main document and verify pdfview window
   And click ekspeder button on checked recepiant
   Then verify "JP Status" as "DF_sakStatus_E" in the "0" journal list
   When logged out from Arkivar
   Then login with Arkivar
   Then select the Sender / Recipients tab
   Then verify "Ekspedrinstatus" as "DF_Esk_Epost" in "0" Motakarlist
   Then verify "Ekspedrinstatus" as "DF_Esk_Epost" in "2" Motakarlist
   And Rightclick "1" th row and select Ekspeeder
   Then I should preview uncheck recepiant
   And click expand button verify document 
   Then click on the main document and verify pdfview window
   And click ekspeder button on checked recepiant
   Then verify "JP Status" as "DF_sakStatus_E" in the "0" journal list
   And Select standardsone refresh button
   Then select the Sender / Recipients tab
   Then verify "Ekspedrinstatus" as "DF_Esk_Epost" in "1" Motakarlist
   Then verify "Ekspedrinstatus" as "DF_Esk_Epost" in "3" Motakarlist
   Then close application process
   
  @WFH-T2745, @CI @Ekspedering @Regression2
   Scenario:WFH-T2745-8 - Ekspedering - Velge vekk vedlegg
   Given Case "WFH-T2745" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T2745" as "Title1" for the document to be added
   Then Enter "WFH-T2745" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_ARK1" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_AG" details and Select Kopi button
   And click on journal avsendre button
   And select "DF_journalRecepiant_LDR" details and Select Kopi button
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   And Clicks "DF_journalDoc_Templete" document
   And clicks Avbryt Button
   And Clicks "sjekkInnOgAvslutt" button
   Then check in the document
   When I attach "existing" document at the bottom
   Then document should be attached
   And select status as "DF_journalStatus_F"
   And click Lagre button
   Then verify "JP Status" as "DF_journalStatus_F" in the "0" journal list
   And Rightclick "1" th row and select Ekspeeder
   Then I should preview uncheck recepiant
   And click expand button verify document 
   Then click on the main document and verify pdfview window
   And click ekspeder button on checked recepiant
   Then verify "JP Status" as "DF_sakStatus_E" in the "0" journal list
   When logged out from Arkivar
   Then login with Arkivar
   Then select the Sender / Recipients tab
   Then verify "Ekspedrinstatus" as "DF_Esk_Epost" in "1" Motakarlist
   Then verify "Ekspedrinstatus" as "DF_Esk_Epost" in "2" Motakarlist
   Then close application process
   
 @WFH-T2746 
   Scenario:WFH-T2746-8- Dispatch - Multiple channels - Document type U - Multiple recipients
   Given Case "WFH-T2746" should be created
   Then Choose "DF_JournalMenuOutgoing" document from document list
   Then Enter "WFH-T2746" as "Title1" for the document to be added
   Then Enter "WFH-T2746" as "Title2" for the document to be added
   And click on journal avsendre button
   And select "DF_journalRecepiant" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_SDP" details
   And click on journal avsendre button
   And select "DF_journalRecepiant_2" details
   And Click on Save button
   And Navigate to "sakdokumenterTab" tab
   And Navigate to "tabOpprettDokument" tab
   And Clicks "DF_journalDoc_Templete" document
   And clicks Avbryt Button
   And Clicks "sjekkInnOgAvslutt" button
   Then document should be attached
   And select status as "DF_sakStatus_F"
   And click Lagre button
   Then verify "JP Status" as "DF_journalStatus_F" in the "0" journal list
   #Then I should Click on EkSpeeder Link
   Then I should preview multiple recepiant 
   And click expand button verify document 
   Then click on the main document and verify pdfview window
   And click ekspeder button
   Then verify "JP Status" as "DF_sakStatus_E" in the "0" journal list
   And verify ekspeder status as "Papir - Lagt i kø for sending"
   Then select history icon and press F5
   
   @WFH-T2808, @CI @Oppgave @Regression2
    Scenario:WFH-T2808-Oppgave 1: Til godkjenning på utgående dokument
    Given Case "WFH-T2808" should be created
    Then Choose "DF_JournalMenuOutgoing" document from document list
    Then Enter "WFH-T2808" as "Title1" for the document to be added
    Then Enter "WFH-T2808" as "Title2" for the document to be added
    And click on journal avsendre button
    And select "DF_journalRecepiant_SBH" details
    And Click on Save button
    And Navigate to "sakdokumenterTab" tab
    And Navigate to "tabOpprettDokument" tab
    And Clicks "DF_journalDoc_Templete_Blank" document
    And Clicks "sjekkInnOgAvslutt" button
    #Then check in the document
    And Navigate to "tabOppagaver" tab
    Then select New oppgave
    Then select oppgave type as "Til godkjenning"
    And verify oppgave type set as "DF_OppagaveType_TilGodjen" in text field
    And select Leggtil button in mottakarlist
    And select "DF_journalRecepiant_LDR" details
    Then verify added "taskMottakar" as "DF_OppagaveMot_LDR" in "0" oppagave mottakarlist
    Then enter comment as "Approved" in oppgave textbox
    Then select ok button
    And verify oppagave value as incresed by "1"
    And Navigate to "tabOppagaver" tab
    Then verify task approved "approvedTasksend" as "currentTask" in "0" oppagave list
    And Navigate to "tabJournalPost" tab
    #Then click on the icon to right of the Shield field
    #And Navigate to "tabTilgangsgruppe" tab 
    #Then click expand button in intern group
    #Then verify created task "tilgangRecepiant" as "DF_OppagaveMot_LDR" in "1" TilgangGroup list
    Then close application process
    And  login with leader
    And Click firstitem in "Mottatt for godkjenning" item
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    Then verify task approved "approvedTasksend" as "currentTask" in "0" oppagave list
    Then verify task approved "exclamation" as "DF_Oppagave_exclamation" in "0" oppagave list
    Then select Behandle oppgave
    Then select oppgave type as "GodKjnningEks" 
    And verify oppgave type set as "DF_OppagaveType_GodjenEksLdr" in text field
    Then select ok button
    And Close print and ekspeder window
    And verify oppagave value as incresed by "2"
    Then click expand button in oppagavelist main view
    And refresh maintreeview panel
    Then close application process
    And login with Arkivar
    And Click firstitem in "Mottatt oppgaver" item
    Then verify "Tittel" as "currentJpTitle" in "0" journal list
    And Navigate to "tabOppagaver" tab
    Then verify task approved "approvedTasksend" as "currentTask" in "1" oppagave list
    Then close application process
  
   
   
   
   
  
   

   
   
   
   
   

    
   
    



    
    
  


    

    
    
    

    

    
    

    
    
    
    
    
     

    
    

    

    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
