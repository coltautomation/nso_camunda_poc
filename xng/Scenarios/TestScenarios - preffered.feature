﻿Feature: TestScenarios

@Demo1
Scenario: Upload File from UI
  Given Launch UI Application for tcid "TC1001"
  Then Upload File from UI
  Then close the chrome Browser
  Given Login to XNG application "TC1001"
  Then Search circuit Id
  Then search or create A end LU circuit
  Then search or create Z end LU circuit
  Then Attach LU circuit with LE circuit
  Given Launch UI Application for tcid "TC1001"
  Then submit A and Z end
  Then click user task
  And click A end sumbit button
  Then verify success message of A end as "Valid_Message UI"
  And click Z end sumbit button
  Then verify success message of B end as "Valid_Message UI"
  Then close the chrome Browser