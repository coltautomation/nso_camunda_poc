﻿Feature: Test Scenario - Stepwise

@Demo2
Scenario: xngCreate circuit and Attach LU in LE
     Given Launch UI Application for tcid "TC1002"
     Then Click Upload XML
     Then Click Choose File
     Then Choose XML file as "UIFilepath" from local
     Then click accept button
     And click search services
     Then enter service Id as "Order_Id"
     And click search button in UI
     Then store the circuitId in variable
     Then close the chrome Browser
     Given Login to XNG application "TC1002"
     And click path inside equipment menu
     Then enter path circuitid as "Circuit_Id"
     Then click search button
     And click edit button
     Then select status as Virtual
     Then click path ETH from left down tree
     Then enter A VLAN ether type as "A_VLANEtherTypeValue"
     Then enter B VLAN ether type as "B_VLANEtherTypeValue"
     Then enter VLAN ID Cust as "VLANCUST Id"  
     And click save button
     Then click newwindow button
     And click path inside equipment menu
     Then click new icon
     Then enter path circuitid as "A_ENDCircuitId"
     Then select category as "LANLINK"
     Then select status as Virtual
     Then choose bandwidth as "1GB"
     Then choose Topology as "Topology_AEND"
     Then choose A side site as "A-Side site_AEND"
     Then choose B side site as "B-Side site_AEND"
     Then Select "Customers" from leftside jtree menuitem
     Then Enter A,Z and ordering side cutomers as "OrderingCustomerAEND"
     Then enter A_side customer value as "A-Side_CustomerAEND"
     Then enter B_side customer value as "B-Side_CustomerAEND"
     Then Select "All Circuit Paths" from leftside jtree menuitem
     Then enter SLA ID as "SLA_IDAEND"
     Then click save button
     Then Select "Elements" from leftside jtree menuitem
     Then click Insert after selection element button
     Then select equipment port in menu
     Then click refine search button
     Then enter equipment id as "AEND_EquipID"
     Then select button equipment search
     Then Select Device from search result as "AEND_EquipID"
     Then click the port in ListofPort as "Port_Aend" in A End
     And click device connectivity in elementlist as "AEND_EquipID"
     Then select "ETH Details" in port tree
     Then click edit buuton in port window
     And enter NSO-MTU as "NSO-MTU"
     And click button save in port window
     Then click save button
     Then Select "Channels" from leftside jtree menuitem
     Then click Insert after selection port button
     And  enter number of channel as "No.of ChannelAEND"
     Then enter channel start from as "Channel_Start_FromAEND"
     Then Choose Bandwidth as "BandWidth" in channel
     And click save button in channel
     Then click save button
     Then click newwindow button
     And click path inside equipment menu
     Then click new icon
     Then enter path circuitid as "Z_ENDCircuitId"
     Then select category as "LANLINK"
     Then select status as Virtual
     Then choose bandwidth as "Bandwidth"
     Then choose Topology as "Topology_ZEND"
     Then choose A side site as "A-Side site_ZEND"
     Then choose B side site as "B-Side site_ZEND"
     Then Select "Customers" from leftside jtree menuitem
     Then Enter A,Z and ordering side cutomers as "OrderingCustomerZEND"
     Then enter A_side customer value as "A-Side_CustomerZEND"
     Then enter B_side customer value as "B-Side_CustomerZEND"
     Then Select "All Circuit Paths" from leftside jtree menuitem
     Then enter SLA ID as "SLA_IDZEND"
     Then click save button
     Then Select "Elements" from leftside jtree menuitem
     Then click Insert after selection element button
     Then select equipment port in menu
     Then click refine search button
     Then enter equipment id as "ZEND_EquipID"
     Then select button equipment search
     Then Select Device from search result as "ZEND_EquipID"
     Then click the port in ListofPort as "Port_Zend" in Z End
     And click device connectivity in elementlist as "ZEND_EquipID"
     Then select "ETH Details" in port tree
     Then click edit buuton in port window
     And enter NSO-MTU as "NSO-MTU"
     And click button save in port window
     Then click save button
     Then Select "Channels" from leftside jtree menuitem
     Then click Insert after selection port button
     And  enter number of channel as "No.of ChannelZEND"
     Then enter channel start from as "Channel_Start_FromZEND"
     Then Choose Bandwidth as "BandWidth" in channel
     And click save button in channel
     Then click save button
     Then click newwindow button
     And click path inside equipment menu
     Then enter path circuitid as "Circuit_Id"
     Then click search button
     And click edit button
     Then select status as Virtual
     Then Select "Elements" from leftside jtree menuitem
     Then click insertbefore element
     And click insertbefore elementberfore
     And click filter in path tree
     Then enter network id in search Path as "A_ENDCircuitId"
     Then enter A side site as "A-Side site_AEND" search box
     Then click search icon
     And choose resultant value as "A_ENDCircuitId"
     Then choose created  LU channel as "Channel"
     Then click save button
     Then click insertbefore element
     And click insertbefore elementberfore
     And click filter in path tree
     Then enter network id in search Path as "Z_ENDCircuitId"
     Then enter Z side site as "B-Side site_ZEND" search box
     Then click search icon
     And choose resultant value as "Z_ENDCircuitId"
     Then choose created  LU channel as "Channel"
     Then click save button
     #Given Launch UI Application for tcid "TC1002"
     #Then click search services
     #Then enter service Id as "Order_Id"
     #Then click search button in UI
     #Then click user task
     #And click A end sumbit button
     #Then verify success message of A end as "Valid_Message UI"
     #And click Z end sumbit button
     #Then verify success message of B end as "Valid_Message UI"
     #Then close the chrome Browser